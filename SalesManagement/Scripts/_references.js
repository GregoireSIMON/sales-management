﻿/// <reference path="jquery-3.4.1.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <autosync enabled="true" />
/// <reference path="../bower_components/bootstrap/dist/js/bootstrap.js" />
/// <reference path="../bower_components/datatables/media/js/jquery.dataTables.js" />
/// <reference path="../bower_components/datatables-responsive/js/dataTables.responsive.js" />
/// <reference path="../bower_components/flot.tooltip/js/jquery.flot.tooltip.js" />
/// <reference path="../bower_components/holderjs/holder.js" />
/// <reference path="../bower_components/jquery/dist/jquery.js" />
/// <reference path="../bower_components/metisMenu/dist/metisMenu.js" />
/// <reference path="../bower_components/mocha/mocha.js" />
/// <reference path="../bower_components/morrisjs/morris.js" />
/// <reference path="../bower_components/raphael/raphael.js" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap-editable.js" />
/// <reference path="bootstrap-switch.js" />
/// <reference path="bootstrap-table.js" />
/// <reference path="bootstrap-table-editable.js" />
/// <reference path="bootstrap-toggle.min.js" />
/// <reference path="bootstrap-tour.js" />
/// <reference path="bootstrap-treeview.js" />
/// <reference path="chart.bundle.min.js" />
/// <reference path="chart-utils.js" />
/// <reference path="datatables.bootstrap.min.js" />
/// <reference path="default.js" />
/// <reference path="dropzonejs.js" />
/// <reference path="expressive.annotations.validate.min.js" />
/// <reference path="faq.js" />
/// <reference path="flot-data.js" />
/// <reference path="fr.js" />
/// <reference path="fullcalendar.min.js" />
/// <reference path="index.js" />
/// <reference path="jquery.bootstrap.wizard.min.js" />
/// <reference path="jquery.datatables.min.js" />
/// <reference path="jquery.hcolumns.min.js" />
/// <reference path="jquery.highlight-4.js" />
/// <reference path="jquery.signalr-2.2.2.min.js" />
/// <reference path="jquery.tagsinput.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-jvectormap-2.0.3.min.js" />
/// <reference path="jquery-jvectormap-europe-mill.js" />
/// <reference path="jquerywrapper.js" />
/// <reference path="modalform.js" />
/// <reference path="moment.min.js" />
/// <reference path="morris-data.js" />
/// <reference path="prettify.js" />
/// <reference path="range-slider.min.js" />
/// <reference path="respond.js" />
/// <reference path="sb-admin-2.js" />
/// <reference path="toastr.min.js" />
/// <reference path="util.js" />
