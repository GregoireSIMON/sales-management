using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;

namespace SalesManagement
{
    public class MvcApplication : HttpApplication
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
        }
        protected void Application_Error()
        {
            HttpContext httpContext = HttpContext.Current;

            if (httpContext != null)
            {
                RequestContext requestContext = ((MvcHandler)httpContext.CurrentHandler).RequestContext;
                Logger.Fatal("Application_Error", httpContext.Error is AggregateException exception ? exception.InnerException : httpContext.Error);
                /* when the request is ajax the system can automatically handle a mistake with a JSON response. then overwrites the default response */
                if (requestContext.HttpContext.Request.IsAjaxRequest())
                {
                    string controllerName = requestContext.RouteData.GetRequiredString("controller");
                    IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
                    IController controller = factory.CreateController(requestContext, controllerName);
                    ControllerContext controllerContext = new ControllerContext(requestContext, (ControllerBase)controller);
                    JsonResult jsonResult = new JsonResult
                    {
                        Data = new { success = false, serverError = httpContext.Response.StatusCode },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    httpContext.Response.Clear();
                    jsonResult.ExecuteResult(controllerContext);
                    httpContext.Response.End();
                }
                else
                {
                    int statusCode = httpContext.Response.StatusCode != 500 ? httpContext.Response.StatusCode : 500;
                    string exceptionDetails = httpContext.Error is AggregateException && httpContext.Error.InnerException != null
                        ? httpContext.Error.InnerException.Message
                        : httpContext.Error.Message;
                    string message = Regex.Replace(exceptionDetails.Substring(0, 500 > exceptionDetails.Length ? exceptionDetails.Length : 500), @"\t|\n|\r", " - ");
                    httpContext.Response.Clear();
                    httpContext.Response.Redirect($"~/Error/?code={statusCode}&message={message}");
                }
            }
        }
    }
}
