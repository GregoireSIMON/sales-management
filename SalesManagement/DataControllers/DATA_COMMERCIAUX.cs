//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SalesManagement.DataControllers
{
    using System;
    using System.Collections.Generic;
    
    public partial class DATA_COMMERCIAUX
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DATA_COMMERCIAUX()
        {
            this.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY = new HashSet<DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY>();
        }
    
        public int IDCOMMERCIAL { get; set; }
        public string PRENOM { get; set; }
        public string NOM { get; set; }
        public string ABREVIATION { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY> DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY { get; set; }
    }
}
