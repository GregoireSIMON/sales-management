﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace SalesManagement
{
    public static class MyAuthentication
    {
        public const string ApplicationCookie = "MyProjectAuthenticationType";
    }

    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = MyAuthentication.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"), // PAGE DE CONNEXION
                Provider = new CookieAuthenticationProvider(),
                CookieName = "SalesManegement", // NOM DU COOKIE
                CookieHttpOnly = true,
                ExpireTimeSpan = TimeSpan.FromHours(4), // TEMPS EN HEURE DE CONNEXION
            });
        }
    }
}