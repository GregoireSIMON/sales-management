﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SalesManagement.DataControllers;

namespace SalesManagement
{
    public class Pagination
    {
        private string libelle;
        private int page;
        private int pageSize;
        private int pageCount;
        private int totalResultCout;
        private string researchType;
        private List<GE> ge = new List<GE>();
        private List<ETB> etb = new List<ETB>();
        //private List<DATA_CORRESPONDANCES_CAP_ETAB_GROUPES> corresp = new List<DATA_CORRESPONDANCES_CAP_ETAB_GROUPES>();

        public string Libelle { get => libelle; set => libelle = value; }
        public int Page { get => page; set => page = value; }
        public int PageSize { get => pageSize; set => pageSize = value; }
        public int PageCount { get => pageCount; set => pageCount = value; }
        public int TotalResultCout { get => totalResultCout; set => totalResultCout = value; }
        //public List<DATA_CORRESPONDANCES_CAP_ETAB_GROUPES> Corresp { get => corresp; set => corresp = value; }
        public List<GE> GE { get => ge; set => ge = value; }
        public List<ETB> ETB { get => etb; set => etb = value; }
        public string ResearchType { get => researchType; set => researchType = value; }
    }
}