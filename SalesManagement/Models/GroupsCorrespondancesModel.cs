﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SalesManagement.DataControllers;

namespace SalesManagement
{
    public class GroupsCorrespondancesModel
    {
        private string libelleGroupe;
        private string libelleSecteur;
        private string libelleTaille;
        private string libelleRegion;
        private string libelleSegment;
        private string nomPrenomCommerial;

        private int idGroupe;
        private int idSecteur;
        private int idTaille;
        private int idRegion;
        private int idSegment;
        private int idCom;

        private DateTime? dateAdd;

        public string LibelleGroupe { get => libelleGroupe; set => libelleGroupe = value; }
        public string LibelleSecteur { get => libelleSecteur; set => libelleSecteur = value; }
        public string LibelleTaille { get => libelleTaille; set => libelleTaille = value; }
        public string LibelleRegion { get => libelleRegion; set => libelleRegion = value; }
        public string LibelleSegment { get => libelleSegment; set => libelleSegment = value; }
        public string NomPrenomCommerial { get => nomPrenomCommerial; set => nomPrenomCommerial = value; }
        public int IdGroupe { get => idGroupe; set => idGroupe = value; }
        public int IdSecteur { get => idSecteur; set => idSecteur = value; }
        public int IdTaille { get => idTaille; set => idTaille = value; }
        public int IdRegion { get => idRegion; set => idRegion = value; }
        public int IdSegment { get => idSegment; set => idSegment = value; }
        public int IdCom { get => idCom; set => idCom = value; }
        public DateTime? DateAddGrp { get => dateAdd; set => dateAdd = value; }
    }
}