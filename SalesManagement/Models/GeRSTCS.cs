﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SalesManagement.DataControllers;

namespace SalesManagement
{
    public class GeRSTCS // Groupe Region Secteur Taille Commercial Segment Etablissement
    {
        private DATA_GROUPES grp = new DATA_GROUPES();
        private CAP_ETAB etb = new CAP_ETAB();
        private List<DATA_REGIONS> listRegion = new List<DATA_REGIONS>();
        private List<DATA_SECTEURS> listSecteur = new List<DATA_SECTEURS>();
        private List<DATA_TAILLES> listTaille = new List<DATA_TAILLES>();
        private List<DATA_COMMERCIAUX> listCom = new List<DATA_COMMERCIAUX>();
        private List<DATA_SEGMENTS> listSegment = new List<DATA_SEGMENTS>();

        public DATA_GROUPES Grp { get => grp; set => grp = value; }
        public List<DATA_REGIONS> ListRegion { get => listRegion; set => listRegion = value; }
        public List<DATA_SECTEURS> ListSecteur { get => listSecteur; set => listSecteur = value; }
        public List<DATA_TAILLES> ListTaille { get => listTaille; set => listTaille = value; }
        public List<DATA_COMMERCIAUX> ListCom { get => listCom; set => listCom = value; }
        public List<DATA_SEGMENTS> ListSegment { get => listSegment; set => listSegment = value; }
        public CAP_ETAB Etb { get => etb; set => etb = value; }
    }
}