﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagement.DataControllers;

namespace SalesManagement.DataControllers
{
    [Authorize]
    public class ComSegment
    {
        private DATA_COMMERCIAUX com;
        private string libelleSegment;

        public DATA_COMMERCIAUX Com { get => com; set => com = value; }
        public string LibelleSegment { get => libelleSegment; set => libelleSegment = value; }
    }
}

