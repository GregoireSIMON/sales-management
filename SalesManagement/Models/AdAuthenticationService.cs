﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Security.Claims;

namespace SalesManagement
{
    public class AdAuthenticationService
    {
        public class AuthenticationResult
        {
            public AuthenticationResult(string errorMessage = null)
            {
                ErrorMessage = errorMessage;
            }

            public string ErrorMessage { get; private set; }

            public bool IsSuccess => String.IsNullOrEmpty(ErrorMessage);
        }

        private readonly IAuthenticationManager _authenticationManager;

        public AdAuthenticationService(IAuthenticationManager authenticationManager)
        {
            _authenticationManager = authenticationManager;
        }

        /// <summary>
        /// Check if username and password matches existing account in AD. 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public AuthenticationResult SignIn(string username, string password)
        {
            // PROD
            //ContextType authenticationType = ContextType.Machine;
            // DEV
            ContextType authenticationType = ContextType.Domain;
            PrincipalContext principalContext = new PrincipalContext(authenticationType);
            bool isAuthenticated;
            UserPrincipal userPrincipal = null;

            try
            {
                isAuthenticated = principalContext.ValidateCredentials(username, password, ContextOptions.Negotiate);
                if (isAuthenticated)
                    userPrincipal = UserPrincipal.FindByIdentity(principalContext, username);
            }
            catch (Exception)
            {
                isAuthenticated = false;
                userPrincipal = null;
            }
            if (!isAuthenticated || userPrincipal == null)
                return new AuthenticationResult("Merci de vérifier vos identifiants, nom d'utilisateur et mot de passe.");
            if (userPrincipal.IsAccountLockedOut())
                return new AuthenticationResult("Votre compte est vérouillé.");
            if (userPrincipal.Enabled.HasValue && userPrincipal.Enabled.Value == false)
                return new AuthenticationResult("Votre compte est inactif.");
            var identity = CreateIdentity(userPrincipal);
            _authenticationManager.SignOut(MyAuthentication.ApplicationCookie);
            _authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);
            return new AuthenticationResult();
        }

        private static IEnumerable<Principal> GetAuthorizationGroups(UserPrincipal user)
        {
            PrincipalSearchResult<Principal> groups = user.GetAuthorizationGroups();
            List<Principal> ret = new List<Principal>();
            var iterGroup = groups.GetEnumerator();

            using (iterGroup)
            {
                while (iterGroup.MoveNext())
                {
                    try
                    {
                        Principal p = iterGroup.Current;
                        ret.Add(p);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            return ret;
        }

        private ClaimsIdentity CreateIdentity(UserPrincipal userPrincipal)
        {
            var identity = new ClaimsIdentity(MyAuthentication.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "Active Directory"));
            identity.AddClaim(new Claim(ClaimTypes.Name, userPrincipal.SamAccountName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userPrincipal.SamAccountName));
            if (!String.IsNullOrEmpty(userPrincipal.EmailAddress))
                identity.AddClaim(new Claim(ClaimTypes.Email, userPrincipal.EmailAddress));
            var groups = GetAuthorizationGroups(userPrincipal);
            foreach (GroupPrincipal group in groups)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, group.Name));
            }
            // add your own claims if you need to add more information stored on the cookie
            return identity;
        }

        public static UserPrincipal GetUser()
        {
            // PROD
            //ContextType authenticationType = ContextType.Machine;
            // DEV
            ContextType authenticationType = ContextType.Domain;
            PrincipalContext principalContext = new PrincipalContext(authenticationType);
            UserPrincipal userPrincipal = null;

            try
            {
                userPrincipal = UserPrincipal.FindByIdentity(principalContext, System.Web.HttpContext.Current.User.Identity.Name);
            }
            catch (Exception)
            {
                userPrincipal = null;
            }

            return userPrincipal;
        }
    }
}