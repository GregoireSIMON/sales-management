﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SalesManagement.DataControllers;

namespace SalesManagement
{
    public class ETB
    {
        private string libelle;
        private int etb_id;
        private string siret;
        private string adresse;
        private string dateAdd;
        private string state;
        private int? idGroupeCorresp;
        private string libelleGroupeCorresp;
        private string dateCorresp;


        public string Libelle { get => libelle; set => libelle = value; }
        public int ETB_ID { get => etb_id; set => etb_id = value; }
        public string Siret { get => siret; set => siret = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string DateAdd { get => dateAdd; set => dateAdd = value; }
        public string State { get => state; set => state = value; }
        public int? IdGroupeCorresp { get => idGroupeCorresp; set => idGroupeCorresp = value; }
        public string LibelleGroupeCorresp { get => libelleGroupeCorresp; set => libelleGroupeCorresp = value; }
        public string DateCorresp { get => dateCorresp; set => dateCorresp = value; }
    }
}