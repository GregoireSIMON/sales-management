﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SalesManagement.DataControllers;

namespace SalesManagement
{
    public class Corresp_ComGrpBU
    {
        private DateTime? dateDebut;
        private DateTime? dateFin;

        private string libelleGroupe;
        private string libelleSegment;

        private int idCom;
        private string nomPrenomCom;

        private int idGroupe;

        public DateTime? DateDebut { get => dateDebut; set => dateDebut = value; }
        public DateTime? DateFin { get => dateFin; set => dateFin = value; }
        public string LibelleGroupe { get => libelleGroupe; set => libelleGroupe = value; }
        public string LibelleSegment { get => libelleSegment; set => libelleSegment = value; }
        public int IdCom { get => idCom; set => idCom = value; }
        public string NomPrenomCom { get => nomPrenomCom; set => nomPrenomCom = value; }
        public int IdGroupe { get => idGroupe; set => idGroupe = value; }
    }
}