﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SalesManagement.DataControllers;

namespace SalesManagement
{
    public class GE 
    {
        private int etb_id;
        private string etb_name;
        private string etb_adr;
        private string etb_adr2;
        private string etb_adr3;
        private string etb_ville;
        private string etb_cp;
        private string etb_tel;
        private string etb_effectif;
        private string etb_naf;
        private string date_AddEtb;


        private string siret;
        private int idGroupe;
        private string libelleGroupe;
        private string libelleRegion;
        private string libelleTaille;
        private string libelleSecteur;
        private string libelleSegment;
        private string date_AddGrp;
        private string nomPrenomCom;

        private string date_AdddEtbGrp;

        public int Etb_id { get => etb_id; set => etb_id = value; }
        public string Etb_name { get => etb_name; set => etb_name = value; }
        public string Siret { get => siret; set => siret = value; }
        public string Siren { get => siret.Substring(0,9) ;}

        public int IdGroupe { get => idGroupe; set => idGroupe = value; }
        public string LibelleGroupe { get => libelleGroupe; set => libelleGroupe = value; }
        public string LibelleRegion { get => libelleRegion; set => libelleRegion = value; }
        public string LibelleTaille { get => libelleTaille; set => libelleTaille = value; }
        public string LibelleSecteur { get => libelleSecteur; set => libelleSecteur = value; }
        public string LibelleSegment { get => libelleSegment; set => libelleSegment = value; }
        public string NomPrenomCom { get => nomPrenomCom; set => nomPrenomCom = value; }
        public string Etb_adr { get => etb_adr; set => etb_adr = value; }
        public string Etb_adr2 { get => etb_adr2; set => etb_adr2 = value; }
        public string Etb_adr3 { get => etb_adr3; set => etb_adr3 = value; }
        public string Etb_ville { get => etb_ville; set => etb_ville = value; }
        public string Etb_cp { get => etb_cp; set => etb_cp = value; }
        public string Etb_tel { get => etb_tel; set => etb_tel = value; }
        public string Etb_effectif { get => etb_effectif; set => etb_effectif = value; }
        public string Etb_naf { get => etb_naf; set => etb_naf = value; }
        public string Date_AddEtb { get => date_AddEtb; set => date_AddEtb = value; }
        public string Date_AddGrp { get => date_AddGrp; set => date_AddGrp = value; }
        public string Date_AddEtbGrp { get => date_AdddEtbGrp; set => date_AdddEtbGrp = value; }
    }

}