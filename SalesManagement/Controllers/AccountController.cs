﻿using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using CustomerAreaBackoffice.Web1.Models;
using SalesManagement.Controllers;
using log4net;

namespace SalesManagement.Controllers
{
    public class AccountController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AccountController));

        [Authorize]

        public ActionResult Denied() => View();

        public ActionResult Login() => View();

        [Authorize]
        public ActionResult Settings() => View();

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return View(model);
            IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
            var authService = new AdAuthenticationService(authenticationManager);
            var authenticationResult = authService.SignIn(model.UserName, model.Password);
            if (authenticationResult.IsSuccess)
            {
                // On est connecté

                

                return RedirectToLocal(returnUrl);
            }
            ModelState.AddModelError("", authenticationResult.ErrorMessage);
            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }

        public virtual ActionResult Logoff()
        {
            IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;

            authenticationManager.SignOut(MyAuthentication.ApplicationCookie);


            Logger.Info("Deconnexion");

            return RedirectToAction("Login", "Account");
        }
    }
}