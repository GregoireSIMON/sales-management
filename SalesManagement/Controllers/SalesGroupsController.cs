﻿using log4net;
using SalesManagement.DataControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesManagement.Controllers
{
    [Authorize]
    public class SalesGroupsController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SalesGroupsController));

        public ActionResult Index() => View(GetListCom());
        public PartialViewResult GetGroups(int idCom) => PartialView("Partial/_SalesGroupsTable_PartialView", GetListCGLByIdCom(idCom));
        public PartialViewResult GetCom() => PartialView("Partial/_AddModal_PartialView", GetListCom());
        public PartialViewResult CheckCorrespondance(int idGroup) => PartialView("Partial/_CheckCorrespondanceModal_PartialView", GetListCGLByIdGroup(idGroup).Where(x => x.DateFin != null).ToList());
        public PartialViewResult GetComGroups(int idCom)
        {
            using (var ctx = new CapData())
            {
                List<int> listIdGroupCorrespondance = new List<int>();
                foreach (var item in ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDCOMMERCIAL == idCom && x.DATE_FIN == null))//correspondance en cours
                    listIdGroupCorrespondance.Add(item.IDGROUPE);

                var result = ctx.DATA_GROUPES.Where(x => !listIdGroupCorrespondance.Contains(x.IDGROUPE)).ToList();

                return PartialView("\\_SelectGroup_PartialView", result);
            }

        } //Recupere les groupes rattachés à un com
      
        public PartialViewResult AddCorrespondanceComGroup(int idGroup, int idCom)
        {
            using (var ctx = new CapData())
            {
                try
                {
                    var legalEntity = ctx.DATA_LEGALENTITY.Where(bu => bu.IDLEGALENTITY != 37 && bu.IDLEGALENTITY != 38).ToList(); // to remove LegalEntity "NC" and LegalEntity "REGION"
                    var com = ctx.DATA_COMMERCIAUX.FirstOrDefault(x => x.IDCOMMERCIAL == idCom);
                    var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);

                    if (idCom != 999)
                    {
                        foreach (var bu in legalEntity)
                        {
                            var toInsert = new DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY
                            {
                                IDGROUPE = idGroup,
                                IDLEGALENTITY = bu.IDLEGALENTITY,
                                IDCOMMERCIAL = idCom,
                                DATE_DEBUT = DateTime.Now,
                                DATA_COMMERCIAUX = ctx.DATA_COMMERCIAUX.FirstOrDefault(x => x.IDCOMMERCIAL == idCom),
                                DATA_GROUPES = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup),
                                DATA_LEGALENTITY = bu
                            };
                            ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Add(toInsert);
                        }
                        ctx.SaveChanges();

                        Logger.Info($"Création correspondance commercial-groupe {com.NOM} {com.PRENOM} - {grp.LIBELLE} | commercial n° {idCom} | groupe n°{idGroup}");
                    }
                }
                catch { }

                return PartialView("Partial/_SalesGroupsTable_PartialView", GetListCGLByIdCom(idCom));
            }
        } //Ajoute une correspondance
        public void DeleteCorrespondance(int idGroup)
        {
            using (var ctx = new CapData())
            {
                var listToDelete = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDGROUPE == idGroup && x.DATE_FIN == null).ToList();
                var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);
                foreach (var toDelete in listToDelete)
                    toDelete.DATE_FIN = DateTime.Now;

                ctx.SaveChanges();

                Logger.Info($"Fin des correspondances commercial-groupe pour {grp.LIBELLE} | groupe n° {idGroup}");
            }

        }  //Supprime les correspondances entre un com et un groupe et en créer une nouvelle
        public PartialViewResult AddNewCorrespondanceComGroup(int idGroup, int idCom)
        {
            DeleteCorrespondance(idGroup);
            return AddCorrespondanceComGroup(idGroup, idCom);
        } //Creer la correspondance entre un com et un groupe
        public PartialViewResult DeleteCorrespondanceComGroup(int idGroup, int idCom)
        {
            using (var ctx = new CapData())
            {
                var listToDelete = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDGROUPE == idGroup && x.IDCOMMERCIAL == idCom).ToList();
                var com = ctx.DATA_COMMERCIAUX.FirstOrDefault(x => x.IDCOMMERCIAL == idCom);
                var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);

                foreach (var toDelete in listToDelete)
                    toDelete.DATE_FIN = DateTime.Now;

                ctx.SaveChanges();
               
                Logger.Info($"Suppression correspondance commercial-groupe  {com.NOM} {com.PRENOM} - {grp.LIBELLE} | commercial n° {idCom} | groupe n° {idGroup}");

                return PartialView("Partial/_SalesGroupsTable_PartialView", GetListCGLByIdCom(idCom));
            }
        } //Ajoute une date de fin à une correspondance

        #region GetList
        public static List<Corresp_ComGrpBU> GetListCGLByIdCom(int idCom)
        {
            using (var ctx = new CapData())
            {
                var listCorresp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDCOMMERCIAL == idCom && x.IDLEGALENTITY == 1).ToList();
                var listCorresp_ComGrpBU = new List<Corresp_ComGrpBU>();
                foreach (var corresp in listCorresp)
                {
                    var comGrpBu = new Corresp_ComGrpBU
                    {
                        DateDebut = corresp.DATE_DEBUT,
                        DateFin = corresp.DATE_FIN,
                        LibelleGroupe = corresp.DATA_GROUPES.LIBELLE,
                        LibelleSegment = corresp.DATA_GROUPES.DATA_SEGMENTS.LIBELLE,
                        IdCom = idCom,
                        IdGroupe = corresp.IDGROUPE
                    };
                    listCorresp_ComGrpBU.Add(comGrpBu);
                }
                return listCorresp_ComGrpBU;
            }
        }
        public static List<Corresp_ComGrpBU> GetListCGLByIdGroup(int idGroup)
        {
            using (var ctx = new CapData())
            {
                var listCorresp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDGROUPE == idGroup && x.IDLEGALENTITY == 1).ToList();
                var listCorresp_ComGrpBU = new List<Corresp_ComGrpBU>();
                foreach (var corresp in listCorresp)
                {
                    var comGrpBu = new Corresp_ComGrpBU
                    {
                        DateDebut = corresp.DATE_DEBUT,
                        DateFin = corresp.DATE_FIN,
                        LibelleGroupe = corresp.DATA_GROUPES.LIBELLE,
                        LibelleSegment = corresp.DATA_GROUPES.DATA_SEGMENTS.LIBELLE,
                        IdCom = corresp.IDCOMMERCIAL,
                        IdGroupe = idGroup
                    };
                    listCorresp_ComGrpBU.Add(comGrpBu);
                }
                return listCorresp_ComGrpBU;
            }
        }
        public static List<DATA_COMMERCIAUX> GetListCom()
        {
            using (var ctx = new CapData())
            {
                return ctx.DATA_COMMERCIAUX.ToList();
            }
        }
        #endregion
    }
}
