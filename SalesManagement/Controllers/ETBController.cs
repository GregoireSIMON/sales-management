﻿using log4net;
using SalesManagement.DataControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace SalesManagement.Controllers
{
    [Authorize]
    public class ETBController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ETBController));

        public ActionResult Index() => View();
        public void CreateGroupForE(string libelle, int idRegion, int idSecteur, int idTaille, int idSegment, int idCom, int etb_id)
        {
            GroupsController g = new GroupsController();
            GEController ge = new GEController();

            int idGroup = g.CreateGroup(libelle, idRegion, idSecteur, idTaille, idSegment, idCom);

            ge.AddCorrespondanceEG(etb_id, idGroup);

            Logger.Info($"Création du groupe {libelle} pour l'établissement {etb_id}");
        }
        public PartialViewResult SearchGroup(string libelle)
        {
            using(CapData ctx = new CapData())
            {
                var result = ctx.DATA_GROUPES.Where(x => x.LIBELLE.Contains(libelle)).ToList();
                List<GE> listGroup = new List<GE>();

                bool isNumeric = int.TryParse(libelle, out int siret);
                if (isNumeric)
                {
                    var etab = ctx.CAP_ETAB.FirstOrDefault(x => x.ETB_SIRET.Contains(libelle.Substring(0, 9)));
                    if (etab != null)
                    {
                        int idGroupe = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.FirstOrDefault(x => x.ETB_ID == etab.ETB_ID).IDGROUPE;
                        result = ctx.DATA_GROUPES.Where(x => x.IDGROUPE == idGroupe).ToList();
                    }
                }
                foreach (var groupe in result)
                {
                    var ge = new GE
                    {
                        LibelleGroupe = groupe.LIBELLE,
                        LibelleRegion = groupe.DATA_REGIONS.REGION,
                        LibelleSecteur = groupe.DATA_SECTEURS.LIBELLE,
                        LibelleSegment = groupe.DATA_SEGMENTS.LIBELLE,
                        LibelleTaille = groupe.DATA_TAILLES.LIBELLE,
                        IdGroupe = groupe.IDGROUPE
                    };
                    listGroup.Add(ge);
                }
                return PartialView("Partial/_SearchGroup_PartialView", listGroup);
            }
        
        }
        public PartialViewResult EG(int ETB_ID)
        {
            using (CapData ctx = new CapData())
            {
                CAP_ETAB etb = ctx.CAP_ETAB.Where(x => x.ETB_ID == ETB_ID).FirstOrDefault();
                List<int> etbIdList = new List<int>();

                GeRSTCS geRSTCS = new GeRSTCS
                {
                    ListCom = ctx.DATA_COMMERCIAUX.ToList(),
                    ListRegion = ctx.DATA_REGIONS.ToList(),
                    ListSecteur = ctx.DATA_SECTEURS.ToList(),
                    ListTaille = ctx.DATA_TAILLES.ToList(),
                    ListSegment = ctx.DATA_SEGMENTS.ToList(),
                    Etb = etb
                };

                if (etb.ETB_SIRET != null)
                    etbIdList = ctx.CAP_ETAB.Where(x => x.ETB_SIRET.Substring(0, 9) == etb.ETB_SIRET.Substring(0, 9)).Select(x => x.ETB_ID).ToList();
                else
                    etbIdList = ctx.CAP_ETAB.Where(x => x.ETB_NAME.Contains(etb.ETB_NAME)).Select(x => x.ETB_ID).ToList();

                var corresp = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.FirstOrDefault(x => etbIdList.Contains(x.ETB_ID) || x.DATA_GROUPES.LIBELLE.Contains(etb.ETB_NAME) || x.DATA_GROUPES.LIBELLE == etb.ETB_NAME);

                if (corresp != null)
                {
                    geRSTCS.Grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == corresp.IDGROUPE);
                }
                else
                {
                    string firstWordEtb = "";

                    if (etb.ETB_NAME.Contains(" "))
                        firstWordEtb = etb.ETB_NAME.Substring(0, etb.ETB_NAME.IndexOf(" "));
                    else
                        firstWordEtb = etb.ETB_NAME;

                    var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.LIBELLE.Contains(firstWordEtb));
                    geRSTCS.Grp = grp;
                }
                return PartialView("Partial/_ETBGroups_Step2_PartialView", geRSTCS);
            }

        } //Recupere le groupe et l'etablissement pour un etb_id
        public PartialViewResult GetETB(string libelle, string capdataId, string date, string startDate, string endDate, int page, int pageSize)
        {
            using (CapData ctx = new CapData())
            {
                List<CAP_ETAB> result = new List<CAP_ETAB>();
                string siret = "", adresse="", state ="", libelleGroupeCorresp ="", dateCorresp ="";
                int? idGroupeCorresp;
                int skip = (page - 1) * pageSize;

                Pagination p = new Pagination
                {
                    Page = page,
                    Libelle = libelle,
                    PageSize = pageSize
                };

                if (libelle != null)
                {
                    result = ctx.CAP_ETAB.Where(x => x.ETB_NAME.Contains(libelle) || x.ETB_SIRET.Contains(libelle) || x.CAP_ADRESSE.ADR_CP.Contains(libelle) || x.CAP_ADRESSE.ADR_VILLE.Contains(libelle) || x.CAP_ADRESSE.ADR_ADR1.Contains(libelle) || x.CAP_ADRESSE.ADR_ADR2.Contains(libelle) || x.CAP_ADRESSE.ADR_ADR3.Contains(libelle)).ToList();
                    p.ResearchType = "libelle";
                }
                else if (date != null)
                {
                    result = ctx.CAP_ETAB.SqlQuery("SELECT * FROM CAP_ETAB C LEFT JOIN DATA_CORRESPONDANCES_CAP_ETAB_GROUPES D ON C.ETB_ID = D.ETB_ID WHERE CONVERT(date, C.FLAG_DATADD) ='" + Convert.ToDateTime(date).ToString("yyyy-MM-dd") + "' ORDER BY D.IDGROUPE ASC").ToList();
                    p.ResearchType = "date";
                    p.Libelle = date;
                }
                else if (startDate != null && endDate != null)
                {
                    result = ctx.CAP_ETAB.SqlQuery("SELECT * FROM CAP_ETAB C LEFT JOIN DATA_CORRESPONDANCES_CAP_ETAB_GROUPES D ON C.ETB_ID = D.ETB_ID WHERE CONVERT(date, C.FLAG_DATADD) BETWEEN '" + Convert.ToDateTime(startDate).ToString("yyyy-MM-dd") + "' AND '" + Convert.ToDateTime(endDate).ToString("yyyy-MM-dd") + "' ORDER BY D.IDGROUPE ASC").ToList();
                    p.ResearchType = "period";
                    p.Libelle = startDate + "," + endDate;
                }
                else
                {
                    result = ctx.CAP_ETAB.SqlQuery("SELECT * FROM CAP_ETAB C LEFT JOIN DATA_CORRESPONDANCES_CAP_ETAB_GROUPES D ON C.ETB_ID = D.ETB_ID WHERE C.ETB_ID LIKE '" + capdataId + "%' ORDER BY D.IDGROUPE ASC").ToList();
                    p.ResearchType = "capdataId";
                    p.Libelle = capdataId;
                };
                p.TotalResultCout = result.Count();

                foreach (var etab in result.Skip(skip).Take(pageSize).ToList())
                {
                    var corresp = etab.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.FirstOrDefault(y => y.ETB_ID == etab.ETB_ID);

                    state = corresp != null ? state = "Rattatché" : state = "Non rattaché";
                    siret = etab.ETB_SIRET != null ? siret = etab.ETB_SIRET : siret = "Non renseigné";
                    adresse = etab.ADR_ID != null ? adresse = etab.CAP_ADRESSE.ADR_ADR1 + " " + etab.CAP_ADRESSE.ADR_CP + " " + etab.CAP_ADRESSE.ADR_VILLE : adresse = "Non renseigné";

                    idGroupeCorresp = corresp != null ? idGroupeCorresp  = corresp.IDGROUPE : idGroupeCorresp = null;
                    dateCorresp = corresp != null ? dateCorresp = corresp.DATE_ADD.Value.ToShortDateString() : dateCorresp = "";
                    libelleGroupeCorresp = corresp != null ? dateCorresp = corresp.DATA_GROUPES.LIBELLE : dateCorresp = "";

                    var etb = new ETB
                    {
                        Siret = siret,
                        Adresse = adresse,
                        DateAdd = etab.FLAG_DATADD.Value.ToShortDateString(),
                        ETB_ID = etab.ETB_ID,
                        Libelle = etab.ETB_NAME,
                        State = state,
                        IdGroupeCorresp = idGroupeCorresp,
                        LibelleGroupeCorresp = libelleGroupeCorresp,
                        DateCorresp = dateCorresp
                    };
                    p.ETB.Add(etb);
                }
                p.PageCount = (int)Math.Ceiling((double)result.Count / pageSize);

                return PartialView("Partial/_InfosETB_PartialView", p);
            }
        } //Recupere un etablissement en fct d'un libelle / capdataid
    }
}
