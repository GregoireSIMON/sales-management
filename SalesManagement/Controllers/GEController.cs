﻿using log4net;
using SalesManagement.DataControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagement.Hubs;

namespace SalesManagement.Controllers
{
    [Authorize]
    public class GEController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GEController));

        public ActionResult Index() => View();

        public PartialViewResult GetAllGE()
        {
            using (var ctx = new CapData())
            {
                var result = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.ToList();
                List<GE> listGE = new List<GE>();
                int x ,y = 0;
                        
                foreach (var corresp in result)
                {
                    x = y * 100 / result.Count;

                    ProgressHub.SendMessage($"Chargement des données {x} %", x);
                    GE ge = new GE
                    {
                        Etb_name = corresp.CAP_ETAB.ETB_NAME,
                        Siret = corresp.CAP_ETAB.ETB_SIRET != null ? corresp.CAP_ETAB.ETB_SIRET : "Non renseigné",
                        LibelleGroupe = corresp.DATA_GROUPES.LIBELLE,
                        LibelleSegment = corresp.DATA_GROUPES.DATA_SEGMENTS.LIBELLE,
                        NomPrenomCom = GetCom(corresp.IDGROUPE) != "" ? GetCom(corresp.IDGROUPE) : "Non renseigné"
                    };
                    listGE.Add(ge);
                    y++;
                }
                
                return PartialView("Partial/_Export", listGE);
            }
        }
        public PartialViewResult GetGE(string libelle, int page, int pageSize)
        {
            using(CapData ctx = new CapData())
            {
                int v, y = 0;

                var result = new List<DATA_CORRESPONDANCES_CAP_ETAB_GROUPES>();

                int skip = (page - 1) * pageSize;

                if (libelle == "")
                    result = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.ToList();
                else
                    result = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.Where(x => x.CAP_ETAB.ETB_NAME.Contains(libelle) || x.CAP_ETAB.ETB_SIRET.Contains(libelle) || x.DATA_GROUPES.LIBELLE.Contains(libelle) || x.DATA_GROUPES.LIBELLE == libelle).OrderBy(x => x.IDCORRESPONDANCE).ToList();

                Pagination p = new Pagination
                {
                    Page = page,
                    Libelle = libelle,
                    PageSize = pageSize,
                    PageCount = (int)Math.Ceiling((double)result.Count / pageSize),
                    ResearchType = "Libelle",
                    TotalResultCout = result.ToList().Count
                };
                if (libelle == "")
                    result = result.Skip(skip).Take(pageSize).ToList();


                foreach (var corresp in result)
                {
                    v = y * 100 / result.Count;
                    ProgressHub.SendMessage($"Chargement des données {v} %", v);

                    GE ge = new GE
                    {
                        Etb_id = corresp.ETB_ID,
                        Etb_name = corresp.CAP_ETAB.ETB_NAME,
                        Siret = corresp.CAP_ETAB.ETB_SIRET != null ? corresp.CAP_ETAB.ETB_SIRET : "Non renseigné",
                        IdGroupe = corresp.DATA_GROUPES.IDGROUPE,
                        LibelleGroupe = corresp.DATA_GROUPES.LIBELLE,
                        LibelleRegion = corresp.DATA_GROUPES.DATA_REGIONS.REGION,
                        LibelleSecteur = corresp.DATA_GROUPES.DATA_SECTEURS.LIBELLE,
                        LibelleTaille = corresp.DATA_GROUPES.DATA_TAILLES.LIBELLE,
                        LibelleSegment = corresp.DATA_GROUPES.DATA_SEGMENTS.LIBELLE,
                        NomPrenomCom = GetCom(corresp.IDGROUPE) != "" ? GetCom(corresp.IDGROUPE) : "Non renseigné",
                        Etb_adr = GetAdr(corresp.ETB_ID, 1) != null ? GetAdr(corresp.ETB_ID, 1) : "Non renseigné",
                        Etb_adr2 = GetAdr(corresp.ETB_ID, 2) != null ? GetAdr(corresp.ETB_ID, 2) : "Non renseigné",
                        Etb_adr3 = GetAdr(corresp.ETB_ID, 3) != null ? GetAdr(corresp.ETB_ID, 3) : "Non renseigné",
                        Etb_ville = corresp.CAP_ETAB.CAP_ADRESSE.ADR_VILLE,
                        Etb_naf = corresp.CAP_ETAB.CAP_NAF.NAF_CODE,
                        Date_AddEtb = corresp.CAP_ETAB.FLAG_DATADD.HasValue ? corresp.CAP_ETAB.FLAG_DATADD.Value.ToShortDateString() : "Non renseigné",
                        Date_AddGrp = corresp.DATA_GROUPES.DATE_ADD.HasValue ? corresp.DATA_GROUPES.DATE_ADD.Value.ToShortDateString() : "Non renseigné",
                        Date_AddEtbGrp = corresp.DATE_ADD.HasValue ? corresp.DATE_ADD.Value.ToShortDateString() : "Non renseigné"
                    };
                    p.GE.Add(ge);
                    y++;
                }

                return PartialView("Partial/_GE_PartialView", p);
            }
          
        }
        public PartialViewResult GetGEByDate(DateTime startDate, DateTime endDate, int page, int pageSize)
        {
            using (CapData ctx = new CapData())
            {
                int v, y = 0;
                var result = new List<DATA_CORRESPONDANCES_CAP_ETAB_GROUPES>();

                int skip = (page - 1) * pageSize;

                result = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.SqlQuery("SELECT * FROM DATA_CORRESPONDANCES_CAP_ETAB_GROUPES WHERE CONVERT(date, DATE_ADD) BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "' ").OrderBy(x => x.IDCORRESPONDANCE).ToList();

                Pagination p = new Pagination
                {
                    Page = page,
                    Libelle = startDate.ToShortDateString() + "," + endDate.ToShortDateString(),
                    PageSize = pageSize,
                    PageCount = (int)Math.Ceiling((double)result.Count / pageSize),
                    ResearchType = "Date"
                };

                foreach (var corresp in result.Skip(skip).Take(pageSize))
                {
                    v = y * 100 / result.Count;
                    ProgressHub.SendMessage($"Chargement des données {v} %", v);

                    GE ge = new GE
                    {
                        Etb_id = corresp.ETB_ID,
                        Etb_name = corresp.CAP_ETAB.ETB_NAME,
                        Siret = corresp.CAP_ETAB.ETB_SIRET != null ? corresp.CAP_ETAB.ETB_SIRET : "Non renseigné",
                        IdGroupe = corresp.DATA_GROUPES.IDGROUPE,
                        LibelleGroupe = corresp.DATA_GROUPES.LIBELLE,
                        LibelleRegion = corresp.DATA_GROUPES.DATA_REGIONS.REGION,
                        LibelleSecteur = corresp.DATA_GROUPES.DATA_SECTEURS.LIBELLE,
                        LibelleTaille = corresp.DATA_GROUPES.DATA_TAILLES.LIBELLE,
                        LibelleSegment = corresp.DATA_GROUPES.DATA_SEGMENTS.LIBELLE,
                        NomPrenomCom = GetCom(corresp.IDGROUPE) != "" ? GetCom(corresp.IDGROUPE) : "Non renseigné",
                        Etb_adr = GetAdr(corresp.ETB_ID, 1) != null ? GetAdr(corresp.ETB_ID, 1) : "Non renseigné",
                        Etb_adr2 = GetAdr(corresp.ETB_ID, 2) != null ? GetAdr(corresp.ETB_ID, 2) : "Non renseigné",
                        Etb_adr3 = GetAdr(corresp.ETB_ID, 3) != null ? GetAdr(corresp.ETB_ID, 3) : "Non renseigné",
                        Etb_ville = corresp.CAP_ETAB.CAP_ADRESSE.ADR_VILLE,
                        Etb_naf = corresp.CAP_ETAB.CAP_NAF.NAF_CODE,
                        Date_AddEtb = corresp.CAP_ETAB.FLAG_DATADD.HasValue ? corresp.CAP_ETAB.FLAG_DATADD.Value.ToShortDateString() : "Non renseigné",
                        Date_AddGrp = corresp.DATA_GROUPES.DATE_ADD.HasValue  ? corresp.DATA_GROUPES.DATE_ADD.Value.ToShortDateString() : "Non renseigné",
                        Date_AddEtbGrp = corresp.DATE_ADD.HasValue ? corresp.DATE_ADD.Value.ToShortDateString() : "Non renseigné"
                    };
                    p.GE.Add(ge);
                    y++;
                }
                p.TotalResultCout = result.ToList().Count;

                return PartialView("Partial/_GE_PartialView", p);
            }
        }
        public PartialViewResult GetListGroup()
        {
            using(CapData ctx =new CapData())
            {
                return PartialView("_SelectGroup_PartialView",ctx.DATA_GROUPES.ToList());
            }
        }
            
        public void AddCorrespondanceEG(int ETB_ID, int idGroup)
        {
            using (CapData ctx = new CapData())
            {
                var etb = ctx.CAP_ETAB.FirstOrDefault(x => x.ETB_ID == ETB_ID);
                var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);

                var toInsert = new DATA_CORRESPONDANCES_CAP_ETAB_GROUPES
                {
                    IDGROUPE = idGroup,
                    ETB_ID = ETB_ID,
                    DATE_ADD = DateTime.Today
                };
                ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.Add(toInsert);
                ctx.SaveChanges();

                Logger.Info($"Création de la correspondance etb-groupe {etb.ETB_NAME} - {grp.LIBELLE} | etb n° {ETB_ID} - idGroupe n° {idGroup}");
            }
        }
        public void DeleteCorrespondanceEG(int ETB_ID, int idGroup)
        {
            using (CapData ctx = new CapData())
            {
                var etb = ctx.CAP_ETAB.FirstOrDefault(x => x.ETB_ID == ETB_ID);
                var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);

                var toDelete = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.FirstOrDefault(x => x.ETB_ID == ETB_ID && x.IDGROUPE == idGroup);
                ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.Remove(toDelete);
                ctx.SaveChanges();

                Logger.Info($"Suppression de la correspondance etb-groupe {etb.ETB_NAME} - {grp.LIBELLE} | etb n° {ETB_ID} - idGroupe n° {idGroup}");
            }
        }
        public void EditCorrespondanceEG(int ETB_ID, int idGroup)
        {
            using (CapData ctx = new CapData())
            {
                var etb = ctx.CAP_ETAB.FirstOrDefault(x => x.ETB_ID == ETB_ID);
                var grp = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);

                var toUpdate = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.FirstOrDefault(x => x.ETB_ID == ETB_ID);

                toUpdate.IDGROUPE = idGroup;
                ctx.SaveChanges();

                Logger.Info($"Modification de la correspondance etb-groupe {etb.ETB_NAME} - {grp.LIBELLE} | etb n° {ETB_ID} - idGroupe n° {idGroup}");
            }
        }
        public string GetCom(int idGroupe)
        {
            using (CapData ctx = new CapData())
            {
                string nom = "", prenom = "";
                var corresp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.FirstOrDefault(x => x.IDGROUPE == idGroupe);

                if (corresp != null)
                {
                    var com = ctx.DATA_COMMERCIAUX.FirstOrDefault(x => x.IDCOMMERCIAL == corresp.IDCOMMERCIAL);
                    nom = com.NOM;
                    prenom = com.PRENOM;
                }

                return nom + prenom;
            }
        }
        public string GetAdr(int etb_id, int adrNumber)
        {
            using (CapData ctx = new CapData())
            {
                string adr = "";
                CAP_ADRESSE capAdresse = capAdresse = ctx.CAP_ETAB.FirstOrDefault(x => x.ETB_ID == etb_id).CAP_ADRESSE;
                switch (adrNumber)
                {
                    case 1:
                        adr = capAdresse.ADR_ADR1 + " " + capAdresse.ADR_CP + " " + capAdresse.ADR_VILLE;
                        break;
                    case 2:
                        adr = capAdresse.ADR_ADR2;
                        break;
                    case 3:
                        adr = capAdresse.ADR_ADR3;
                        break;
                }
                return adr;
            }
        }
    }
}
