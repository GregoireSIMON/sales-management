﻿using log4net;
using SalesManagement.DataControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesManagement.Controllers
{
    [Authorize]
    public class GroupsController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GroupsController));

        public ActionResult Index() => View(GetGroups());
        public void EditGroup(int idGroup, string libelle, int idRegion, int idTaille, int idSecteur, int idSegment, int idCom)
        {
            using(CapData ctx = new CapData())
            {
                var toUpdate = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);
                var corresp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.FirstOrDefault(x => x.IDGROUPE == idGroup && x.DATE_FIN == null);

                string oldLibelle = toUpdate.LIBELLE;

                SalesGroupsController s = new SalesGroupsController();

                if (corresp != null && idCom != corresp.IDCOMMERCIAL) // s'il existe une correspondance entre le commercial et le groupe ET que le commercial est différent de celui de la liaison
                {
                    if (idCom != 999) //Si le commercial est renseigné
                    {
                        s.AddNewCorrespondanceComGroup(idGroup, idCom);
                    }
                    else //Si le commercial est non renseigné
                    {
                        var listToUpdate = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDGROUPE == toUpdate.IDGROUPE && x.DATE_FIN == null).ToList();
                        foreach (var correspondance in listToUpdate)
                            correspondance.DATE_FIN = DateTime.Today;
                    }
                }
                else if (idCom != 999) // il y en a pas
                {
                    s.AddCorrespondanceComGroup(idGroup, idCom);
                }

                toUpdate.LIBELLE = libelle;
                toUpdate.IDREGION = idRegion;
                toUpdate.IDTAILLE = idTaille;
                toUpdate.IDSECTEUR = idSecteur;
                toUpdate.IDSEGMENT = idSegment;

                ctx.SaveChanges();

                Logger.Info($"Modification du groupe {oldLibelle} | id n° { idGroup}");
            }
        } //Modifie un groupe
        public int CreateGroup(string libelle, int idRegion, int idSecteur, int idTaille, int idSegment, int idCom)
        {
            using(CapData ctx = new CapData())
            {
                var toInsertGrp = new DATA_GROUPES
                {
                    LIBELLE = libelle,
                    IDREGION = idRegion,
                    IDSECTEUR = idSecteur,
                    IDTAILLE = idTaille,
                    IDSEGMENT = idSegment,
                    DATE_ADD = DateTime.Today
                };
                ctx.DATA_GROUPES.Add(toInsertGrp);
                ctx.SaveChanges();

                SalesGroupsController s = new SalesGroupsController();
                s.AddCorrespondanceComGroup(toInsertGrp.IDGROUPE, idCom);

                Logger.Info($"Création du groupe {libelle} | id n° {toInsertGrp.IDGROUPE}");

                return toInsertGrp.IDGROUPE;
            }
        }
        public ActionResult AddGroup()
        {
            using (CapData ctx = new CapData())
            {
                GeRSTCS geRSTCS = new GeRSTCS
                {
                    ListCom = ctx.DATA_COMMERCIAUX.ToList(),
                    ListRegion = ctx.DATA_REGIONS.ToList(),
                    ListSecteur = ctx.DATA_SECTEURS.ToList(),
                    ListTaille = ctx.DATA_TAILLES.ToList(),
                    ListSegment = ctx.DATA_SEGMENTS.ToList()
                };

                return PartialView("Partial/_AddGroup_PartialView", geRSTCS);
            }
        }
        public string GetNbCorresp(int id)
        {
            using (CapData ctx = new CapData())
            {
                int nbCorrespCom = 0, nbCorrespETB = 0;

                nbCorrespCom = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDGROUPE == id && x.IDLEGALENTITY == 1 && x.DATE_FIN == null).Count();
                nbCorrespETB = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.Where(x => x.IDGROUPE == id).Count();

                return "" + nbCorrespCom + "," + nbCorrespETB;
            }
        }
        public void  DeleteGroup(int id)
        {
            using (CapData ctx = new CapData())
            {
                var groupe = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == id);

                var saleGrp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDGROUPE == id).ToList();
                var etbGrp = ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.Where(x => x.IDGROUPE == id).ToList();

                foreach (var corresp in saleGrp)
                    ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Remove(corresp);
                foreach (var corresp in etbGrp)
                    ctx.DATA_CORRESPONDANCES_CAP_ETAB_GROUPES.Remove(corresp);

                ctx.DATA_GROUPES.Remove(groupe);
                ctx.SaveChanges();

                Logger.Info($"Suppression du groupe {groupe.LIBELLE} | id n° {id}");
            }
        }
        public ActionResult GetSizeInfo(int idTaille)
        {
            using (CapData ctx = new CapData())
            {
                var result = ctx.DATA_TAILLES.Where(x => x.IDTAILLE == idTaille).FirstOrDefault();
                return PartialView("Partial/_SizeInfos", result);
            }
        }
        public PartialViewResult GetInfosGroup(int idGroup, string partialView)
        {
            using (var ctx = new CapData())
            {
                var result = ctx.DATA_GROUPES.FirstOrDefault(x => x.IDGROUPE == idGroup);
                GeRSTCS grpRSTC = new GeRSTCS
                {
                    Grp = result,
                    ListCom = ctx.DATA_COMMERCIAUX.ToList(),
                    ListRegion = ctx.DATA_REGIONS.ToList(),
                    ListSecteur = ctx.DATA_SECTEURS.ToList(),
                    ListTaille = ctx.DATA_TAILLES.ToList(),
                    ListSegment = ctx.DATA_SEGMENTS.ToList()
                };

                return PartialView(partialView, grpRSTC);
            }
        } //Recupere les infos d'un groupe
        public ActionResult GetAllGroups() => PartialView("Partial/_GetGroups", GetGroups());
        public ActionResult GetGroups(DateTime startDate, DateTime endDate)
        {
            using (CapData ctx = new CapData())
            {
                var listIdGroupe = ctx.DATA_GROUPES.SqlQuery("SELECT * FROM DATA_GROUPES WHERE CONVERT(date, DATE_ADD) BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "' ORDER BY IDGROUPE ASC").Select(x => x.IDGROUPE).ToList();

                var listGroupes = ctx.DATA_GROUPES.Where(x => listIdGroupe.Contains(x.IDGROUPE)).ToList();
                var listCorrespondance = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.DATE_FIN == null && listIdGroupe.Contains(x.IDGROUPE)).ToList();

                return PartialView("Partial/_GetGroups", GetGroupsCorresp(listGroupes, listCorrespondance));
            }
        }

        #region GetList
        public static List<GroupsCorrespondancesModel> GetGroups()
        {
            using (CapData ctx = new CapData())
            {
                var listGroupes = ctx.DATA_GROUPES.ToList();
                var listCorrespondance = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.DATE_FIN == null).ToList();
                return GetGroupsCorresp(listGroupes, listCorrespondance);
            }
        }
        public static List<GroupsCorrespondancesModel> GetGroupsCorresp(List<DATA_GROUPES> listGroupes, List<DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY> listCorrespondance)
        {
            var sList = new List<GroupsCorrespondancesModel>();

            string nomPrenomCom;
            int idCom;

            foreach (var groupe in listGroupes)
            {
                var corresp = listCorrespondance.FirstOrDefault(x => x.IDGROUPE == groupe.IDGROUPE);
                nomPrenomCom = corresp != null ? nomPrenomCom = corresp.DATA_COMMERCIAUX.NOM + " " + corresp.DATA_COMMERCIAUX.PRENOM : nomPrenomCom = "Non renseigné";
                idCom = corresp != null ? idCom = corresp.IDCOMMERCIAL : idCom = 999;

                var groupeCorrespondance = new GroupsCorrespondancesModel
                {
                    IdGroupe = groupe.IDGROUPE,
                    LibelleGroupe = groupe.LIBELLE,
                    LibelleRegion = groupe.DATA_REGIONS.REGION,
                    LibelleSecteur = groupe.DATA_SECTEURS.LIBELLE,
                    LibelleSegment = groupe.DATA_SEGMENTS.LIBELLE,
                    LibelleTaille = groupe.DATA_TAILLES.LIBELLE,
                    IdRegion = groupe.IDREGION,
                    IdSecteur = groupe.IDSECTEUR,
                    IdSegment = groupe.IDSEGMENT,
                    IdTaille = groupe.IDTAILLE,
                    IdCom = idCom,
                    NomPrenomCommerial = nomPrenomCom,
                    DateAddGrp = groupe.DATE_ADD
                };
                sList.Add(groupeCorrespondance);
            }
            return sList;
        }
        #endregion
    }
}
