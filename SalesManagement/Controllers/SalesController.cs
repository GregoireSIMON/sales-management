﻿using log4net;
using SalesManagement.DataControllers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesManagement.Controllers
{
    [Authorize]
    public class SalesController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SalesController));

        public ActionResult Index() => View(GetComSegment());
        public PartialViewResult AddSale(string lastName, string firstName)
        {
            using (var ctx = new CapData())
            {
                var sale = new DATA_COMMERCIAUX
                {
                    NOM = lastName.ToUpper(),
                    PRENOM = firstName.ToUpper()
                };

                ctx.DATA_COMMERCIAUX.Add(sale);
                ctx.SaveChanges();

                Logger.Info($"Création du commercial {lastName} {firstName}");

                return PartialView("Partial/_SalesTable_PartialView", GetComSegment());
            }
        }
        public PartialViewResult EditSale(int id, string lastName, string firstName)
        {
            using (var ctx = new CapData())
            {
                var sale = ctx.DATA_COMMERCIAUX.FirstOrDefault(x => x.IDCOMMERCIAL == id);
                string oldLastName = sale.NOM;
                string oldfirstName = sale.PRENOM;

                sale.NOM = lastName.ToUpper();
                sale.PRENOM = firstName.ToUpper();

                ctx.SaveChanges();

                Logger.Info($"Modification du commercial {oldLastName} {oldfirstName} en {lastName} {firstName}");

                return PartialView("Partial/_SalesTable_PartialView", GetComSegment());
            }
        }
        public PartialViewResult DeleteSale(int id)
        {
            using (var ctx = new CapData())
            {
                var sale = ctx.DATA_COMMERCIAUX.FirstOrDefault(x => x.IDCOMMERCIAL == id);

                var saleGrp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDCOMMERCIAL == id).ToList();
                foreach (var corresp in saleGrp)
                {
                    ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Remove(corresp);
                }
                ctx.DATA_COMMERCIAUX.Remove(sale);
                ctx.SaveChanges();

                Logger.Info($"Suppression du commercial {sale.NOM} {sale.PRENOM}");

                return PartialView("Partial/_SalesTable_PartialView", GetComSegment());
            }
        }
        public int GetNbCorresp(int id)
        {
            using (var ctx = new CapData())
            {
                var nbCorresp = 0;
                nbCorresp = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDCOMMERCIAL == id && x.IDLEGALENTITY == 1).Count();

                return nbCorresp;
            }
        }

        #region GetList
        public static List<ComSegment> GetComSegment()
        {
            using (var ctx = new CapData())
            {
                List<ComSegment> listComSegment = new List<ComSegment>();
                List<DATA_COMMERCIAUX> result = ctx.DATA_COMMERCIAUX.ToList();
                ComSegment c;
                string libelleSegment;

                foreach (var com in result)
                {
                    var listIdGroupe = ctx.DATA_CORRESPONDANCES_COMMERCIAUX_GROUPES_LEGALENTITY.Where(x => x.IDCOMMERCIAL == com.IDCOMMERCIAL).Select(x => x.IDGROUPE).ToList();
                    if (listIdGroupe.Count != 0)
                    {
                        var listIdSegment = ctx.DATA_GROUPES.Where(x => listIdGroupe.Contains(x.IDGROUPE)).Select(x => x.IDSEGMENT).ToList();
                        var communSegmentId = listIdSegment.GroupBy(x => x).OrderByDescending(g => g.Count()).SelectMany(g => g).First();
                        libelleSegment = ctx.DATA_SEGMENTS.FirstOrDefault(x => x.IDSEGMENT == communSegmentId).LIBELLE;
                    }
                    else
                        libelleSegment = "Non renseigné";

                    c = new ComSegment()
                    {
                        Com = com,
                        LibelleSegment = libelleSegment
                    };
                    listComSegment.Add(c);
                }
                return listComSegment;
            }
        }
        #endregion

    }
}