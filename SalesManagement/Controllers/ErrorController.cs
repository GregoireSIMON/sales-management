﻿using System;
using System.Web.Mvc;

namespace CustomerAreaBackoffice.Web1.Controllers
{
    [Authorize]
    public class ErrorController : Controller
    {
        public ActionResult Index(string message, string code)
        {
            if (Int64.TryParse(code, out long statusCode))
                ViewBag.HttpStatusCode = statusCode == 200 ? 500 : statusCode;
            ViewBag.ErrorMessage = message;

            return View();
        }
    }
}