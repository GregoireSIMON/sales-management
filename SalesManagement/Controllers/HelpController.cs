﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesManagement.Controllers
{
    public class HelpController : Controller
    {
        [Authorize]
        public ActionResult Index() => View();
    }
}
