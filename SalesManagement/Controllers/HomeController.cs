﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesManagement.DataControllers;

namespace SalesManagement.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index() => View();
    }
}