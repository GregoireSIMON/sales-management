﻿using Microsoft.AspNet.SignalR;

namespace SalesManagement.Hubs
{
    public class ProgressHub : Hub
    {
        public string message = "Chargement des données";
        public int count = 1;
    
        public static void SendMessage(string msg, int count)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ProgressHub>();
            hubContext.Clients.All.sendMessage(string.Format(msg),count);
        }
        public void GetCountAndMessage()
        {
            Clients.All.sendMessage(string.Format(message),count);
        }
    }
}